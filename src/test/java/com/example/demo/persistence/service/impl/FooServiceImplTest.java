package com.example.demo.persistence.service.impl;

import com.example.demo.persistence.model.Foo;
import com.example.demo.persistence.repository.FooRepository;
import com.example.demo.persistence.service.FooService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FooServiceImplTest {

    @Mock
    private FooRepository fooRepository;

    @InjectMocks
    private FooServiceImpl fooServiceImpl;

    @Test
    void findById() {
        var foores = new Foo("hello");
        foores.setId(1L);
        foores.setVersion(10);
        when(fooRepository.findById(1L)).thenReturn(Optional.of(foores));
        var fooval = fooServiceImpl.findById(1L);

        assertThat(fooval.getName()).isEqualTo(foores.getName());
    }
}