package com.example.demo.web.controller;

import com.example.demo.persistence.model.Foo;
import com.example.demo.persistence.repository.FooRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class FooControllerIT {

    @Autowired
    private FooRepository fooRepository;

    @Autowired
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        var value = new Foo("michiel");
        value.setVersion(2);
        fooRepository.save(value);
    }

    @Test
    void findAll() throws Exception {
        mvc.perform(get("/foo")).andExpect(status().isOk()).andDo(print());
    }
}