package com.example.demo.web.util;

import com.example.demo.web.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;

/**
 * Simple static methods to be called at the start of your own methods to verify correct arguments and state. If the Precondition fails, an {@link HttpStatus} code is thrown
 */
public final class RestPreconditions {

    private RestPreconditions() {
    }

    /**
     * Check if some value was found, otherwise throw exception.
     *
     * @param resource has value true if found, otherwise false
     * @throws ResourceNotFoundException if expression is false, means value not found.
     */
    public static <T> T checkFound(final T resource) {
        if (resource == null) {
            throw new ResourceNotFoundException();
        }

        return resource;
    }

}
