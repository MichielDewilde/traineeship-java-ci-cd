package com.example.demo.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @GetMapping
    public String helloWorld() {
        return "Hello world, Axxes Traineeship 2021!";
    }
}
