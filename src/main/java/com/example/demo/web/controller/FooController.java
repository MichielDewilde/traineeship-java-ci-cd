package com.example.demo.web.controller;

import com.example.demo.persistence.model.Foo;
import com.example.demo.persistence.service.FooService;
import com.example.demo.web.util.RestPreconditions;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@RequestMapping(value = "/foo")
public class FooController {

    private final FooService service;

    public FooController(final FooService service) {
        this.service = service;
    }

    @GetMapping(value = "/{id}")
    public Foo findById(@PathVariable("id") final Long id,
                        final HttpServletResponse response) {
        return RestPreconditions.checkFound(service.findById(id));
    }

    @GetMapping
    public List<Foo> findAll() {
        return service.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Foo create(@NotNull @RequestBody final Foo resource) {
        return service.create(resource);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Foo update(@PathVariable("id") final Long id,
                      @NotNull @RequestBody final Foo resource) {
        RestPreconditions.checkFound(service.findById(id));
        return service.update(resource);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") final Long id) {
        service.deleteById(id);
    }
}
