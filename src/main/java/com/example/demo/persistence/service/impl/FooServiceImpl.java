package com.example.demo.persistence.service.impl;

import com.example.demo.persistence.model.Foo;
import com.example.demo.persistence.repository.FooRepository;
import com.example.demo.persistence.service.FooService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FooServiceImpl implements FooService {

    private final FooRepository repository;

    public FooServiceImpl(final FooRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
    public Foo findById(final long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Foo> findAll() {
        return repository.findAll();
    }

    @Override
    public Foo create(final Foo foo) {
        return repository.save(foo);
    }

    @Override
    public Foo update(final Foo foo) {
        return repository.save(foo);
    }

    @Override
    public void deleteById(final long id) {
        repository.deleteById(id);
    }
}
