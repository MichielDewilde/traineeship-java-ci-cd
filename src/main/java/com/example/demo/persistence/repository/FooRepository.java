package com.example.demo.persistence.repository;

import com.example.demo.persistence.model.Foo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FooRepository extends JpaRepository<Foo, Long> {
}
